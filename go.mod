module go-quiz

go 1.14

require (
	github.com/daviddengcn/go-colortext v0.0.0-20180409174941-186a3d44e920
	github.com/dixonwille/wlog/v3 v3.0.1
	github.com/dixonwille/wmenu/v5 v5.0.0
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
)
