package main

import (
    "database/sql"
    "fmt"
    "github.com/daviddengcn/go-colortext"
    "github.com/dixonwille/wlog/v3"
    "github.com/dixonwille/wmenu/v5"
    _ "github.com/mattn/go-sqlite3"
    "log"
)

type Question struct {
    Id int
    Question string
    Propositions map[int]string
    Difficulte string
    Anecdote string
}

var categories map[int]string

func main() {
    db, err := sql.Open("sqlite3", "./sqlite.db")

	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	rows, err := db.Query("select id, name from categories")
    if err != nil {
        log.Fatal(err)
    }
    defer rows.Close()

    categories = make(map[int]string);

    for rows.Next() {
        var id int
        var name string
        err = rows.Scan(&id, &name)
        if err != nil {
            log.Fatal(err)
        }
        categories[id] = name
    }
    err = rows.Err()
    if err != nil {
        log.Fatal(err)
    }

    ct.ChangeColor(ct.Red, false, ct.Yellow, false)
    fmt.Print(">> BIENVENUE DANS LE QUIZ, ALLEZ C'EST PARTIII !! <<")
    ct.ResetColor()
    fmt.Println()

    askCategory()
}


func askCategory() {
    fmt.Println("")
    fmt.Println("Choix de la catégorie")
    fmt.Println("")
    menu := wmenu.NewMenu("Choix ?")
    menu.AddColor(wlog.Green, wlog.Blue, wlog.Yellow, wlog.Red)
    menu.Action(func (c []wmenu.Opt) error {
        askQuestion(c[0]);
        return nil
    })

    for i := 1; i <= len(categories); i++ {
        menu.Option(categories[i], i, false, nil)
    }
    err := menu.Run()
    if err != nil{
       fmt.Println("Error")
    }
}

func askQuestion(c wmenu.Opt) {
    db, err := sql.Open("sqlite3", "./sqlite.db")

    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    stmt, err := db.Prepare("select id, question, prop1, prop2, prop3, prop4, difficulte, anecdote " +
        "from questions where category_id = ? order by random() limit 1")
    if err != nil {
        log.Fatal(err)
    }
    defer stmt.Close()

    var question Question
    propositions := []string{"", "", "", ""}

    err = stmt.QueryRow(c.Value).Scan(&question.Id,
                                      &question.Question,
                                      &propositions[0],
                                      &propositions[1],
                                      &propositions[2],
                                      &propositions[3],
                                      &question.Difficulte,
                                      &question.Anecdote)

//    doneQuestions = doneQuestions + ", " + strconv.Itoa(question.Id)
    if err != nil {
        log.Fatal(err)
    }

    question.Propositions = make(map[int]string)
    for i, value := range propositions {
        question.Propositions[i] = value
    }

    fmt.Println("")
    fmt.Println("Question '" + c.Text + "' :")
    fmt.Println(question.Question)
    fmt.Println("")
    menu := wmenu.NewMenu("Réponse ?")
    menu.AddColor(wlog.Green, wlog.Blue, wlog.Yellow, wlog.Red)
    menu.Action(func (q []wmenu.Opt) error {
        if (q[0].Value == 0) {
            ct.ChangeColor(ct.White, false, ct.Green, false)
            fmt.Print("  BONNE RÉPONSE !  ")
            ct.ResetColor()
            fmt.Println("")
        } else {
            ct.ChangeColor(ct.White, false, ct.Red, false)
            fmt.Print("  FAUX !  ")
            ct.ResetColor()
            fmt.Println("")
            fmt.Println("La réponse était : " + question.Propositions[0])
        }
        fmt.Println(question.Anecdote)
        askCategory();
        return nil
    })

    for i, value := range question.Propositions {
        menu.Option(value, i, false, nil)
    }

    err = menu.Run()
    if err != nil{
       fmt.Println("Error")
    }
}
